package com.example.demo.dao;
import com.example.demo.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

/**
 * DAO interface implementation, using the JPA interface
 */
@Repository
public class CitiesDAOImpl implements ICitiesDAO {

    @Autowired
    ICitiesJPA citiesJPA;

    @Override
    public List<City> searchAll() {
        return citiesJPA.findAll();
    }

    @Override
    public City searchCityById(Integer idCity) {
        Optional<City> optional = citiesJPA.findById(idCity);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

    @Override
    public void saveCity(City city) {
        citiesJPA.save(city);
    }

    @Override
    public void deleteCity(Integer idCity) {
        citiesJPA.deleteById(idCity);
    }

    @Override
    public void updateCity(City city) {
        citiesJPA.save(city);
    }
}