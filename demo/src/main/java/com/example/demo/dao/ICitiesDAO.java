package com.example.demo.dao;
import com.example.demo.model.City;
import java.util.List;

/**
 * DAO interface with CRUD methods
 */
public interface ICitiesDAO {
    List<City> searchAll();

    City searchCityById(Integer id);

    void saveCity(City city);

    void deleteCity(Integer idCity);

    void updateCity(City city);
}