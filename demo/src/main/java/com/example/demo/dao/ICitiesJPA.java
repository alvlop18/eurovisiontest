package com.example.demo.dao;
import com.example.demo.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

/**
 * Java Persistance Api interface used for accessing the database entities
 */
public interface ICitiesJPA extends JpaRepository<City, Integer> {

}