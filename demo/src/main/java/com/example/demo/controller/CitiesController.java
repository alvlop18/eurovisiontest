package com.example.demo.controller;
import com.example.demo.model.City;
import com.example.demo.service.ICitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * The RestController will be in charge of providing the service operations as REST resources
 */
@RestController
@RequestMapping("/api/")
public class CitiesController {

    @Autowired
    ICitiesService citiesService;

    @GetMapping("/cities")
    public List<City> searchAll() {
        return citiesService.searchAll();
    }

    @GetMapping("/cities/{id}")
    public City searchCityById(@PathVariable("id") Integer id) {
        return citiesService.searchCityById(id);
    }

    @GetMapping("/cities/longestSequence")
    public List<City> longestSequence() {
        return citiesService.longestSequence();
    }

    @PostMapping(value = "/cities", produces = MediaType.TEXT_PLAIN_VALUE)
    public String saveCity(@RequestBody City city) {
        return String.valueOf(citiesService.saveCity(city));
    }

    @PutMapping("/cities")
    public void updateCity(@RequestBody City city) {
        citiesService.updateCity(city);
    }

    @DeleteMapping(value = "/cities/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String deleteCity(@PathVariable("id") Integer id) {
        return String.valueOf(citiesService.deleteCity(id));
    }
}