package com.example.demo.service;
import com.example.demo.dao.ICitiesDAO;
import com.example.demo.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service interface implementation, using the DAO interface
 */
@Service
public class CitiesServiceImpl implements ICitiesService {

    @Autowired
    ICitiesDAO citiesDAO;

    @Override
    public List<City> searchAll() {
        return citiesDAO.searchAll();
    }

    @Override
    public City searchCityById(Integer idCity) {
        return citiesDAO.searchCityById(idCity);
    }

    /**
     * Longest Sequence algorithm
     * @return array with the solution
     */
    @Override
    public List<City> longestSequence() {
        List<City> cities = citiesDAO.searchAll();

        // We first order the cities alphabetically
        cities.sort(new Comparator<City>() {
            @Override
            public int compare(City c1, City c2) {
                return  c1.getName().compareTo(c2.getName());
            }
        });

        int longestSequenceSize = 1;
        int currentLength = 1;
        int position = -1;
        int currentHighestNumberOfSequence = -1;

        // For each starting position...
        for (int i = 0; i < cities.size(); i++) {
            currentLength = 1;
            currentHighestNumberOfSequence = cities.get(i).getIdCity();
            // ... we traverse the list looking for its sequence length
            for (int j = i + 1; j < cities.size(); j++) {
                if (cities.get(j).getIdCity() > currentHighestNumberOfSequence) {
                    currentLength++;
                    currentHighestNumberOfSequence = cities.get(j).getIdCity();
                }
            }
            // If the current sequence is longer than the longest so far, we update the size and its starting position
            if (currentLength > longestSequenceSize) {
                longestSequenceSize = currentLength;
                position = i;
            }
        }

        // Now that we have the first position of the longest sequence in the list, we fill in a new list with it
        List<City> solution = new ArrayList<City>();
        int currentHighestNumberInSolution = -1;
        for (int k = position; k < cities.size(); k++) {
            // We add to the list the next city which has a higher number than the current one
            if (cities.get(k).getIdCity() > currentHighestNumberInSolution) {
                solution.add(cities.get(k));
                currentHighestNumberInSolution = cities.get(k).getIdCity();
            }
        }

        return solution;
    }

    @Override
    public boolean saveCity(City city) {
        if (citiesDAO.searchCityById(city.getIdCity())==null) {
            citiesDAO.saveCity(city);
            return true;
        }
        return false;
    }

    @Override
    public void updateCity(City city) {
        if (citiesDAO.searchCityById(city.getIdCity())!=null) {
            citiesDAO.updateCity(city);
        }
    }

    @Override
    public boolean deleteCity(Integer idCity) {
        if (citiesDAO.searchCityById(idCity)!=null) {
            citiesDAO.deleteCity(idCity);
            return true;
        }
        return false;
    }
}
