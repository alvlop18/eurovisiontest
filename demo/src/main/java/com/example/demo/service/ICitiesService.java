package com.example.demo.service;
import com.example.demo.model.City;
import java.util.List;

/**
 * The methods in charge of implementing the business logic and linking with the data layer (DAO)
 *  */
public interface ICitiesService {
    List<City> searchAll();

    City searchCityById(Integer id);

    List<City> longestSequence();

    boolean saveCity(City city);

    void updateCity(City city);

    boolean deleteCity(Integer idCity);
}
