package com.example.demo.model;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Entity class for the cities, with id and name attributes and their corresponding getters and setters
 */
@Entity
@Table(name = "cities")
public class City implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    public City() {
    }

    public Integer getIdCity() {
        return id;
    }

    public void setIdCity(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof City)) return false;
        City city = (City) o;
        return Objects.equals(id, city.id);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}